package com.miq.bushfire.controller;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.miq.bushfire.engine.VictoriaIncidentsService;
import com.miq.bushfire.entity.VictoriaIncident;

@RestController
@CrossOrigin
public class VictoriaIncidentsController {

  @Autowired
  VictoriaIncidentsService incidentsService;


  static final class Uri {
    private Uri() {
    }

    public static final String victoriaFeed = "/feed-data";
  }

  @RequestMapping(value = Uri.victoriaFeed,
                  method = RequestMethod.POST,
                  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<VictoriaIncident>> victoriaBushfireDataSave()
      throws IOException, JSONException {
    return (new ResponseEntity<>(incidentsService.saveIncidents(), HttpStatus.OK));
  }

  @RequestMapping(value = Uri.victoriaFeed,
                  method = RequestMethod.GET,
                  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<VictoriaIncident>> getvictoriaBushfireData() {
    return (new ResponseEntity<>(incidentsService.getIncidents(), HttpStatus.OK));
  }
}
