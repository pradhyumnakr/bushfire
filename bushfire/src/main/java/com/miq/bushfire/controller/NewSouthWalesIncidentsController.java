package com.miq.bushfire.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.miq.bushfire.engine.NewSouthWalesIncidentsService;

@RestController
@CrossOrigin
public class NewSouthWalesIncidentsController {

  @Autowired
  NewSouthWalesIncidentsService newSouthWalesIncidentsService;


  static final class Uri {
    private Uri() {
    }

    public static final String newSouthWalesFeed = "/nsw-feed-data";
  }

  @RequestMapping(value = Uri.newSouthWalesFeed,
                  method = RequestMethod.GET,
                  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> getvictoriaBushfireData() throws IOException {
    return new ResponseEntity<>(newSouthWalesIncidentsService.getNSWIncidents(), HttpStatus.OK);
  }
}
