package com.miq.bushfire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class BushFire {

  public static void main(String[] args) {
    SpringApplication.run(BushFire.class, args);
  }

}
