package com.miq.bushfire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miq.bushfire.entity.VictoriaIncident;

public interface VictoriaIncidentRepository extends JpaRepository<VictoriaIncident, Integer> {
}
