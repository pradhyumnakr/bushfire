package com.miq.bushfire.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "incidents")
public class VictoriaIncident {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer id;

  @Column(name = "incidentNo")
  private Integer incidentNo;

  @Column(name = "lastUpdateDateTime")
  private String lastUpdateDateTime;

  @Column(name = "originDateTime")
  private String originDateTime;

  @Column(name = "incidentType")
  private String incidentType;

  @Column(name = "incidentStatus")
  private String incidentStatus;

  @Column(name = "incidentSize")
  private String incidentSize;

  @Column(name = "name")
  private String name;

  @Column(name = "territory")
  private String territory;

  @Column(name = "resourceCount")
  private Integer resourceCount;

  @Column(name = "latitude")
  private Double latitude;

  @Column(name = "longitude")
  private Double longitude;

  @Column(name = "eventCode")
  private String eventCode;

  @Column(name = "fireDistrict")
  private String fireDistrict;

  @Column(name = "municipality")
  private String municipality;

  @Column(name = "category1")
  private String category1;

  @Column(name = "category2")
  private String category2;

  @Column(name = "feedType")
  private String feedType;

  @Column(name = "agency")
  private String agency;

  @Column(name = "originStatus")
  private String originStatus;

  @Column(name = "createdDt")
  private String createdDt;

  @Column(name = "lastUpdatedDt")
  private long lastUpdatedDt;

  @Column(name = "lastUpdatedDtStr")
  private String lastUpdatedDtStr;

  @Column(name = "originDateTimeStr")
  private String originDateTimeStr;

  @Column(name = "catg1cssClass")
  private String catg1CssClass;

  @Column(name = "incidentSizeFmt")
  private String incidentSizeFmt;

  @Column(name = "type")
  private String type;

  @Column(name = "incidentLocation")
  private String incidentLocation;

  public String getIncidentLocation() {
    return incidentLocation;
  }

  public void setIncidentLocation(String incidentLocation) {
    this.incidentLocation = incidentLocation;
  }

  public VictoriaIncident() {

  }

  public Integer getIncidentNo() {
    return incidentNo;
  }

  public void setIncidentNo(Integer incidentNo) {
    this.incidentNo = incidentNo;
  }

  public String getLastUpdateDateTime() {
    return lastUpdateDateTime;
  }

  public void setLastUpdateDateTime(String lastUpdateDateTime) {
    this.lastUpdateDateTime = lastUpdateDateTime;
  }

  public String getOriginDateTime() {
    return originDateTime;
  }

  public void setOriginDateTime(String originDateTime) {
    this.originDateTime = originDateTime;
  }

  public String getIncidentType() {
    return incidentType;
  }

  public void setIncidentType(String incidentType) {
    this.incidentType = incidentType;
  }

  public String getIncidentStatus() {
    return incidentStatus;
  }

  public void setIncidentStatus(String incidentStatus) {
    this.incidentStatus = incidentStatus;
  }

  public String getIncidentSize() {
    return incidentSize;
  }

  public void setIncidentSize(String incidentSize) {
    this.incidentSize = incidentSize;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTerritory() {
    return territory;
  }

  public void setTerritory(String territory) {
    this.territory = territory;
  }

  public Integer getResourceCount() {
    return resourceCount;
  }

  public void setResourceCount(Integer resourceCount) {
    this.resourceCount = resourceCount;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public String getEventCode() {
    return eventCode;
  }

  public void setEventCode(String eventCode) {
    this.eventCode = eventCode;
  }

  public String getFireDistrict() {
    return fireDistrict;
  }

  public void setFireDistrict(String fireDistrict) {
    this.fireDistrict = fireDistrict;
  }

  public String getMunicipality() {
    return municipality;
  }

  public void setMunicipality(String municipality) {
    this.municipality = municipality;
  }

  public String getCategory1() {
    return category1;
  }

  public void setCategory1(String category1) {
    this.category1 = category1;
  }

  public String getCategory2() {
    return category2;
  }

  public void setCategory2(String category2) {
    this.category2 = category2;
  }

  public String getFeedType() {
    return feedType;
  }

  public void setFeedType(String feedType) {
    this.feedType = feedType;
  }

  public String getAgency() {
    return agency;
  }

  public void setAgency(String agency) {
    this.agency = agency;
  }

  public String getOriginStatus() {
    return originStatus;
  }

  public void setOriginStatus(String originStatus) {
    this.originStatus = originStatus;
  }

  public String getCreatedDt() {
    return createdDt;
  }

  public void setCreatedDt(String createdDt) {
    this.createdDt = createdDt;
  }

  public long getLastUpdatedDt() {
    return lastUpdatedDt;
  }

  public void setLastUpdatedDt(long lastUpdatedDt) {
    this.lastUpdatedDt = lastUpdatedDt;
  }

  public String getLastUpdatedDtStr() {
    return lastUpdatedDtStr;
  }

  public void setLastUpdatedDtStr(String lastUpdatedDtStr) {
    this.lastUpdatedDtStr = lastUpdatedDtStr;
  }

  public String getOriginDateTimeStr() {
    return originDateTimeStr;
  }

  public void setOriginDateTimeStr(String originDateTimeStr) {
    this.originDateTimeStr = originDateTimeStr;
  }

  public String getCatg1CssClass() {
    return catg1CssClass;
  }

  public void setCatg1CssClass(String catg1CssClass) {
    this.catg1CssClass = catg1CssClass;
  }

  public String getIncidentSizeFmt() {
    return incidentSizeFmt;
  }

  public void setIncidentSizeFmt(String incidentSizeFmt) {
    this.incidentSizeFmt = incidentSizeFmt;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "Incident{" + "incidentNo=" + incidentNo + ", lastUpdateDateTime='" + lastUpdateDateTime
        + '\'' + ", originDateTime='" + originDateTime + '\'' + ", incidentType='" + incidentType
        + '\'' + ", incidentStatus='" + incidentStatus + '\'' + ", incidentSize='" + incidentSize
        + '\'' + ", name='" + name + '\'' + ", territory='" + territory + '\'' + ", resourceCount="
        + resourceCount + ", latitude=" + latitude + ", longitude=" + longitude + ", eventCode='"
        + eventCode + '\'' + ", fireDistrict='" + fireDistrict + '\'' + ", municipality='"
        + municipality + '\'' + ", category1='" + category1 + '\'' + ", category2='" + category2
        + '\'' + ", feedType='" + feedType + '\'' + ", agency='" + agency + '\''
        + ", originStatus='" + originStatus + '\'' + ", createdDt='" + createdDt + '\''
        + ", lastUpdatedDt='" + lastUpdatedDt + '\'' + ", lastUpdatedDtStr='" + lastUpdatedDtStr
        + '\'' + ", originDateTimeStr='" + originDateTimeStr + '\'' + ", catg1CssClass='"
        + catg1CssClass + '\'' + ", incidentSizeFmt='" + incidentSizeFmt + '\'' + ", type='" + type
        + '\'' + '}';
  }
}