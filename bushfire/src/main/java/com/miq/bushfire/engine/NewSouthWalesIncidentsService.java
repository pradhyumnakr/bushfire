package com.miq.bushfire.engine;

import java.io.IOException;


import org.springframework.stereotype.Service;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.xml.XmlPage;


@Service
public class NewSouthWalesIncidentsService {

  public String getNSWIncidents() throws IOException {
    WebClient webClient = new WebClient();
    webClient.getOptions().setJavaScriptEnabled(false);
    webClient.getOptions().setCssEnabled(false);
    webClient.getOptions().setUseInsecureSSL(true);

    XmlPage xmlPage = webClient.getPage("http://www.rfs.nsw.gov.au/feeds/majorIncidents.xml");

    NodeList nodes = xmlPage.getElementsByTagName("description");
    for (int i = 0; i < nodes.getLength(); i++) {
      Element element = (Element) nodes.item(i);
      System.out.println("Data : " + getCharacterDataFromElement(element));
    }
    return xmlPage.toString();
  }

  public String getCharacterDataFromElement(Element e) {
    Node child = e.getFirstChild();
    if (child instanceof CharacterData) {
      CharacterData cd = (CharacterData) child;
      return cd.getData();
    }
    return "";
  }
}
