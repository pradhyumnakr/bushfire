package com.miq.bushfire.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.miq.bushfire.entity.VictoriaIncident;
import com.miq.bushfire.repository.VictoriaIncidentRepository;

@Service
public class VictoriaIncidentsService {
  @Autowired
  private VictoriaIncidentRepository victoriaIncidentRepository;

  private String readAll(Reader rd) throws IOException {
    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }

  public JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
    InputStream is = new URL(url).openStream();
    try {
      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String jsonText = readAll(rd);
      JSONObject json = new JSONObject(jsonText);
      return json;
    } finally {
      is.close();
    }
  }

  public List<VictoriaIncident> saveIncidents() throws IOException, JSONException {
    List<VictoriaIncident> victoriaIncidents = new ArrayList<>();
    JSONObject jsonObject =
        readJsonFromUrl("https://data.emergency.vic.gov.au/Show?pageId=getIncidentJSON");
    Iterator key = jsonObject.keys();
    while (key.hasNext()) {
      String result = (String) key.next();
      for (int i = 0; i < jsonObject.getJSONArray(result).length(); i++) {
        VictoriaIncident victoriaIncident = new VictoriaIncident();
        JSONObject object = (JSONObject) jsonObject.getJSONArray(result).get(i);
        victoriaIncident.setOriginDateTime((String) object.get("originDateTime"));
        victoriaIncident.setIncidentNo((Integer) object.get("incidentNo"));
        victoriaIncident.setLatitude((Double) object.get("latitude"));
        victoriaIncident.setIncidentType((String) object.get("incidentType"));
        victoriaIncident.setMunicipality((String) object.get("municipality"));
        victoriaIncident.setType((String) object.get("type"));
        victoriaIncident.setIncidentSize((String) object.get("incidentSize"));
        victoriaIncident.setIncidentSizeFmt((String) object.get("incidentSizeFmt"));
        victoriaIncident.setIncidentLocation((String) object.get("incidentLocation"));
        victoriaIncident.setIncidentStatus((String) object.get("incidentStatus"));
        victoriaIncident.setLongitude((Double) object.get("longitude"));
        victoriaIncident.setCategory2((String) object.get("category2"));
        victoriaIncident.setLastUpdateDateTime((String) object.get("lastUpdateDateTime"));
        victoriaIncident.setAgency((String) object.get("agency"));
        victoriaIncident.setCategory1((String) object.get("category1"));
        victoriaIncident.setLastUpdatedDt((long) object.get("lastUpdatedDt"));
        victoriaIncident.setCatg1CssClass((String) object.get("catg1CssClass"));
        victoriaIncident.setOriginDateTime((String) object.get("originDateTimeStr"));
        victoriaIncident.setEventCode((String) object.get("eventCode"));
        victoriaIncident.setLastUpdatedDtStr((String) object.get("lastUpdatedDtStr"));
        victoriaIncident.setCreatedDt(object.get("createdDt").toString());
        victoriaIncident.setResourceCount((Integer) object.get("resourceCount"));
        victoriaIncident.setFeedType((String) object.get("feedType"));
        victoriaIncident.setName((String) object.get("name"));
        victoriaIncident.setFireDistrict((String) object.get("fireDistrict"));
        victoriaIncident.setOriginStatus((String) object.get("originStatus"));
        victoriaIncident.setTerritory((String) object.get("territory"));
        victoriaIncidents.add(victoriaIncident);
        //incidentRepository.save(incident);
        System.out.println(object);
      }
    }
    return victoriaIncidents;
  }

  public List<VictoriaIncident> getIncidents() {
    return victoriaIncidentRepository.findAll();
  }
}
